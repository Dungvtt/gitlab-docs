############################
#
# Image the contains the dependencies to run the lints.
# It downloads the gitlab-docs repository based on the
# branch the Docker image is invoked from.
# Based on Alpine.
#
############################

FROM registry.gitlab.com/gitlab-org/gitlab-docs:base
MAINTAINER GitLab Documentation Team

# Install vale
# We need to build it from source, see https://github.com/errata-ai/vale/issues/128

ENV VALE_VERSION=2.1.0
ENV GOBIN=/usr/local/bin

RUN apk add --no-cache wget zip tar make && \
    wget https://github.com/errata-ai/vale/archive/v${VALE_VERSION}.tar.gz && \
    tar -xvzf v${VALE_VERSION}.tar.gz && \
    cd vale-${VALE_VERSION} && \
    make && \
    make install && \
    cd .. && \
    rm -r vale-${VALE_VERSION} && \
    rm v${VALE_VERSION}.tar.gz

# Set up needed environment variables that are called with --build-arg when
# the Docker image is built (see .gitlab-ci.yml).
ARG CI_COMMIT_REF_NAME
# If CI_COMMIT_REF_NAME is not set (local development), set it to master
ENV CI_COMMIT_REF_NAME ${CI_COMMIT_REF_NAME:-master}

WORKDIR /tmp

RUN yarn global add markdownlint-cli && yarn cache clean

RUN wget https://gitlab.com/gitlab-org/gitlab-docs/-/archive/$CI_COMMIT_REF_NAME/gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2 \
  && tar xvjf gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2 \
  && mv gitlab-docs-$CI_COMMIT_REF_NAME gitlab-docs \
  && rm gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2

RUN cd gitlab-docs \
  && yarn install --frozen-lockfile \
  && NOKOGIRI_USE_SYSTEM_LIBRARIES=1 bundle install --jobs 4
