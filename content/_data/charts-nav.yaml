## READ BEFORE EDITING THIS FILE ##
#
### Rules
# - Do not start the URL with "/"
# - Do not add anchors to the links
# - If the URL ends with index.html, leave it empty (e.g. section_url: 'index.html' --> section_url: '')
#
### Options
# - Use "ee_only: true" for categories and docs available only in EE.
#   - When "ee_only: true", use `ee_tier:` to indicate the tiers the feature is available in:
#     - For Ultimate: `ee_tier: 'GitLab Ultimate and GitLab.com Gold'`
#     - For Premium: `ee_tier: 'GitLab Premium, GitLab.com Silver, and higher tiers'`
#     - For Starter: `ee_tier: 'GitLab Starter, GitLab.com Bronze, and higher tiers'`
#     - For self-managed only: `ee_tier: 'GitLab Starter and higher tiers. Not available in GitLab.com'`
#     - For GitLab.com only: `ee_tier: 'GitLab.com Silver only. Not available in GitLab self-managed instances'`
# - Use "external_url: true" for external URLs in docs and categories

sections:
  - section_title: GitLab Helm Charts Docs
    section_url: ''
  - section_title: Install
    section_url: 'installation/'
    section_categories:
      - category_title: Required tools
        category_url: 'installation/tools.html'
      - category_title: Cloud cluster preparation
        category_url: 'installation/cloud/'
        docs:
          - doc_title: EKS
            doc_url: 'installation/cloud/eks.html'
          - doc_title: GKE
            doc_url: 'installation/cloud/gke.html'
          - doc_title: OpenShift
            doc_url: 'installation/cloud/openshift.html'
      - category_title: Deploy
        category_url: 'installation/deployment.html'
      - category_title: Upgrade
        category_url: 'installation/upgrade.html'
      - category_title: Migrate from Omnibus
        category_url: 'installation/migration/'
      - category_title: Version mappings
        category_url: 'installation/version_mappings.html'
  - section_title: Configure
    section_url: 'charts/'
    section_categories:
      - category_title: Globals
        category_url: 'charts/globals.html'
      - category_title: GitLab subcharts
        category_url: 'charts/gitlab/'
        docs:
          - doc_title: Gitaly chart
            doc_url: 'charts/gitlab/gitaly/'
          - doc_title: GitLab Exporter chart
            doc_url: 'charts/gitlab/gitlab-exporter/'
          - doc_title: GitLab Runner chart
            doc_url: 'charts/gitlab/gitlab-runner/'
          - doc_title: GitLab Shell chart
            doc_url: 'charts/gitlab/gitlab-shell/'
          - doc_title: Migrations chart
            doc_url: 'charts/gitlab/migrations/'
          - doc_title: Sidekiq chart
            doc_url: 'charts/gitlab/sidekiq/'
          - doc_title: Unicorn chart
            doc_url: 'charts/gitlab/unicorn/'
      - category_title: Minio chart
        category_url: 'charts/minio/'
      - category_title: Nginx chart
        category_url: 'charts/nginx/'
      - category_title: Redis chart
        category_url: 'charts/redis/'
      - category_title: Redis HA chart
        category_url: 'charts/redis-ha/'
      - category_title: Registry chart
        category_url: 'charts/registry/'
      - category_title: Advanced
        category_url: 'advanced/'
        docs:
          - doc_title: External database
            doc_url: 'advanced/external-db/'
          - doc_title: External Gitaly
            doc_url: 'advanced/external-gitaly/'
          - doc_title: External Nginx
            doc_url: 'advanced/external-nginx/'
          - doc_title: External object storage
            doc_url: 'advanced/external-object-storage/'
          - doc_title: External Redis
            doc_url: 'advanced/external-redis/'
          - doc_title: Persistent volumes
            doc_url: 'advanced/persistent-volumes/'
  - section_title: Troubleshoot
    section_url: 'troubleshooting/'
  - section_title: Contribute
    section_url: 'development/'
